package ru.t1.ktubaltseva.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.t1.ktubaltseva.tm.component.Bootstrap;
import ru.t1.ktubaltseva.tm.configuration.LoggerConfig;


public class LoggerApp {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
        context.registerShutdownHook();
    }

}