package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable final String token) {
        super(token);
    }

}
