package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.IWBS;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelWBS extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(name = "name")
    protected String name = "";

    @Nullable
    @Column(name = "description")
    protected String description;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    public AbstractUserOwnedModelWBS(@NotNull final String name) {
        this.name = name;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final User user,
            @NotNull final String name
    ) {
        this.user = user;
        this.name = name;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
