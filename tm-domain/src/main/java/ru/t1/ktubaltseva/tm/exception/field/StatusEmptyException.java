package ru.t1.ktubaltseva.tm.exception.field;

public class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}