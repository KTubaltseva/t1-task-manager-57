package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;

import static ru.t1.ktubaltseva.tm.constant.ProjectTaskTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractServiceTest {
    @NotNull
    private static IProjectTaskDTOService service;

    @NotNull
    private static IUserDTOService userService;

    @NotNull
    private static ITaskDTOService taskService;

    @NotNull
    private static IProjectDTOService projectService;

    @NotNull
    private static IUserDTOService getUserService() {
        return context.getBean(IUserDTOService.class);
    }

    @NotNull
    private static ITaskDTOService getTaskService() {
        return context.getBean(ITaskDTOService.class);
    }

    @NotNull
    private static IProjectDTOService getProjectService() {
        return context.getBean(IProjectDTOService.class);
    }

    @NotNull
    private static IProjectTaskDTOService getService() {
        return context.getBean(IProjectTaskDTOService.class);
    }

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        service = getService();
        userService = getUserService();
        taskService = getTaskService();
        projectService = getProjectService();
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.remove(USER_1);
        userService.remove(USER_2);
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear(USER_1.getId());
        taskService.clear(USER_2.getId());
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    public void bindTaskToProject() throws AbstractException {
        @Nullable final UserDTO user = USER_1;
        @Nullable final TaskDTO task = USER_1_TASK_1;
        @Nullable final ProjectDTO project = USER_1_PROJECT_1;
        projectService.add(project);
        taskService.add(task);

        Assert.assertThrows(AuthRequiredException.class, () -> service.bindTaskToProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final TaskDTO taskBinded = service.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskBinded);
        Assert.assertEquals(task.getId(), taskBinded.getId());
        Assert.assertEquals(project.getId(), task.getProjectId());
    }

    @Test
    @SneakyThrows
    public void unbindTaskFromProject() throws AbstractException {
        @Nullable final UserDTO user = USER_1;
        @Nullable final TaskDTO task = USER_1_TASK_1;
        @Nullable final ProjectDTO project = USER_1_PROJECT_1;
        task.setProjectId(project.getId());
        projectService.add(project);
        taskService.add(task);

        Assert.assertThrows(AuthRequiredException.class, () -> service.unbindTaskFromProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final TaskDTO taskUnbind = service.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskUnbind);
        Assert.assertEquals(task.getId(), taskUnbind.getId());
        Assert.assertEquals(NULL_PROJECT_ID, taskUnbind.getProjectId());

        task.setProjectId(project.getId());
    }

}
