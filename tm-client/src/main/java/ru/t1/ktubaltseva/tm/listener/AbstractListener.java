package ru.t1.ktubaltseva.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.ITokenService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected ILoggerService loggerService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getCommandName();

    @NotNull
    public abstract String getDescription();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception;

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getCommandName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += ": " + description;
        return result;
    }

}
