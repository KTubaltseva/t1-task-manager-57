package ru.t1.ktubaltseva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.listener.AbstractListener;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    private final String NAME = "command-list";

    @NotNull
    private final String DESC = "Display command list.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-cl";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@commandListListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        for (@NotNull final AbstractListener listener : listeners) {
            @NotNull final String name = listener.getCommandName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
