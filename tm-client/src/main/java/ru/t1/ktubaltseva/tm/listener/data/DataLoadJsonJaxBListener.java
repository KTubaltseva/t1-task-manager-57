package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadJsonJaxBRequest;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadJsonJaxBListener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-load-json-jaxb";

    @NotNull
    private final String DESC = "Load data from json file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataLoadJsonJaxBListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[LOAD JSON DATA]");
        @NotNull final DataLoadJsonJaxBRequest request = new DataLoadJsonJaxBRequest(getToken());
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

}
