package ru.t1.ktubaltseva.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.listener.system.AbstractSystemListener;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private List<AbstractSystemListener> commands;

    @NotNull
    private final File folder = new File("./");

    public void init() {
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String filename = file.getName();
            final boolean check = commands.contains(filename);
            if (check) {
                try {
                    file.delete();
                    publisher.publishEvent(new ConsoleEvent(filename));
                } catch (@NotNull final Exception e) {
                    loggerService.error(e);
                }
            }
        }
    }

}
