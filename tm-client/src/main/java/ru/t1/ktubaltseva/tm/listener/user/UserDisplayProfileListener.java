package ru.t1.ktubaltseva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.dto.request.user.UserDisplayProfileRequest;
import ru.t1.ktubaltseva.tm.dto.response.user.UserDisplayProfileResponse;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class UserDisplayProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "user-display-profile";

    @NotNull
    private final String DESC = "Display user profile.";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@userDisplayProfileListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DISPLAY USER PROFILE]");
        @NotNull final UserDisplayProfileRequest request = new UserDisplayProfileRequest(getToken());
        @NotNull final UserDisplayProfileResponse response = getAuthEndpoint().getProfile(request);
        @Nullable final UserDTO user = response.getUser();
        displayUser(user);
    }

}
